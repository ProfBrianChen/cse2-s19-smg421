/// Stella Garriga smg421 lab 05 3/1/19
//// Obj.: print out a twist onto the screen 

import java.util.Scanner; //imports scanner 

public class TwistGenerator{
  
  public static void main(String[] args) { 
  // main method that is required for all programs
   Scanner myScanner = new Scanner( System.in ) ; //declaring the instance of the scanner and calling the scanner instructor 
    
   System.out.println("Enter a length in the form of a positive integer"); //prompts the user for a positive integer 
    
   while (!myScanner.hasNextInt()){
      System.out.println("Enter an integer");
      String junkWord = myScanner.next(); 
   }
    
   int length = myScanner.nextInt();
   String a = "";
   String b = "";
   String c = "";

    int i = 0;
   while (i < length){
     switch (i%3){
       case 2:
         a+= "/";
         b+= " ";
         c+= "\\";
         break; 
       case 1:
         a+= " ";
         b+= "X";
         c+= " ";
         break; 
       case 0:
         a+= "\\";
         b+= " ";
         c+= "/";
         break;
     }
     i = i + 1;
   }
    
   System.out.println(a); //prints line 1 of the design
   System.out.println(b); //prints line 2 of the design
   System.out.println(c); //prints line 3
    
  } //end of method 
    
} // end of class