//////// Stella Garriga smg421 2/8/19
/////// Lab 03 : We will be using scanner class to  to obtain from the user the original cost of the check, 
/////the percentage tip they wish to pay, and the number of ways the check will be split. 
////Then determine how much each person in the group needs to spend in order to pay the check.

import java.util.Scanner; // Imports scanner class so that it can be used in the code 
//declaring the class 
public class Check{
  
  public static void main(String[] args) { 
  // main method that is required for all programs
   Scanner myScanner = new Scanner( System.in ) ; //declaring the instance of the scanner and calling the scanner instructor 
    
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //prompts the user for the original cost of the check 
    double checkCost = myScanner.nextDouble () ; //accepts the user input
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx) : "); //prompts the user for the tip percentage 
    double tipPercent = myScanner.nextDouble () ; //accepts the user input 
    tipPercent /= 100 ; //we want to conver the tip percentage to a decimal 
      
    System.out.print("Enter the number of people that went out to dinner: "); //prompt the user for the number of people that went out to dinner
    int numPeople = myScanner.nextInt () ; //accepts the user input 
    
    double totalCost; //declares the class and enables us to store the total cost of the meal 
    double costPerPerson; //declares the class of and enables us to store the total each person must pay for the meal
    int dollars, //for storing dollar amounts 
          dimes, pennies; //for storing digits to the right of the decimal points 
    
    totalCost = checkCost * (1 + tipPercent) ; //calculates the total cost of the meal inclusing tip
    costPerPerson = totalCost / numPeople; //calculates the total each person must pay 
    
    dollars=(int)costPerPerson; //gets the whole amount, dropping the decimal fraction
    dimes=(int) (costPerPerson * 10) % 10; //gets dimes amount 
    pennies=(int) (costPerPerson * 100) % 10; //gets pennies amount 
    
    System.out.print("Each person in the group owes $" + dollars + '.' + dimes + pennies); //tells the user how much each person owes 
    
    
  } //end of main method 
} //end of class