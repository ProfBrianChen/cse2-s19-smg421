///// Stella Garriga 
///// smg421 
////// Hw 06 Due 3/19/19
//// Obj.:

import java.util.Scanner; //imports scanner for use

public class Network { //main method
  
  public static void main(String[] args){ //class 
    
    Scanner myScanner = new Scanner (System.in); //declaring the instance of the scanner and calling the instructor 
    
    //HEIGHT
    System.out.println("Input your desired height:"); //prompts the user for a positive integer
    
    while (!myScanner.hasNextInt()){ //condition that indicates an error and constrution of first while loop
       System.out.println("Error: Enter an integer."); //indicates error to the user
       String junkWord = myScanner.next(); // flushes incorrect input from the scanner
    } //end of while loop for checking the input
    
    int height = myScanner.nextInt(); //saves input from scanner as height
    
    while (height<1){ //loop checks that the integer is positive 
       System.out.println("Error: Enter a postive integer.");
       height = myScanner.nextInt();
    }
    
    //WIDTH
    System.out.println("Input your desired width:"); //prompts the user for a positive integer
    
    while (!myScanner.hasNextInt()){ //condition that indicates an error and constrution of first while loop
       System.out.println("Error: Enter an integer."); //indicates error to the user
       String junkWord = myScanner.next(); // flushes incorrect input from the scanner
    } //end of while loop for checking the input
    
    int width = myScanner.nextInt(); //saves input from scanner as height
    
    while (width<1){ //loop checks that the integer is positive 
       System.out.println("Error: Enter a postive integer.");
       width = myScanner.nextInt();
    }
    
    //SQUARE SIZE 
    System.out.println("Input square size:"); //prompts the user for a positive integer
    
    while (!myScanner.hasNextInt()){ //condition that indicates an error and constrution of first while loop
       System.out.println("Error: Enter an integer."); //indicates error to the user
       String junkWord = myScanner.next(); // flushes incorrect input from the scanner
    } //end of while loop for checking the input
    
    int squareSize = myScanner.nextInt(); //saves input from scanner as height
    
    while (squareSize<1){ //loop checks that the integer is positive 
       System.out.println("Error: Enter a postive integer.");
       height = myScanner.nextInt();
    }
    
    //EDGE LENGTH
    System.out.println("Input edge length:"); //prompts the user for a positive integer
    
    while (!myScanner.hasNextInt()){ //condition that indicates an error and constrution of first while loop
       System.out.println("Error: Enter an integer."); //indicates error to the user
       String junkWord = myScanner.next(); // flushes incorrect input from the scanner
    } //end of while loop for checking the input
    
    int edgeLength = myScanner.nextInt(); //saves input from scanner as height
    
    while (edgeLength<1){ //loop checks that the integer is positive 
       System.out.println("Error: Enter a postive integer.");
       edgeLength = myScanner.nextInt();
    }
    
    //System.out.println("Height:" + height);
    //System.out.println("Width:" + width);
    //System.out.println("Square size:" + squareSize);
    //System.out.println("Edge Length:" + edgeLength);
  }
}