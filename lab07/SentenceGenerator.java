//// Stella Garriga 
//// smg421 3/22/19
//// Obj. generate random sentences 

import java.util.Random; //imports random statement into the program 

public class SentenceGenerator { //beginning of class
  
  //MAIN METHOD
  public static void main (String[] args) { 
  
  //declare new random generator 
  Random randomGenerator = new Random(); 
  //initalize random generator 
  int randomInt = randomGenerator.nextInt(10) +1; 
  adjectiveGenerator(randomInt);
  
  System.out.print ("The" + adjectiveGenerator(randomInt));
  randomInt = randomGenerator.nextInt(10)+ 1; 
  
  System.out.print (adjectiveGenerator(randomInt));
  
  System.out.print(nounGenerator(randomInt)); 
  
  System.out.print (verbGenerator( randomInt) +" the");
  
  randomInt = randomGenerator.nextInt(10) + 1;   
  System.out.print (adjectiveGenerator(randomInt));
  
  randomInt = randomGenerator.nextInt(10)+ 1;
  System.out.print(nounGenerator(randomInt));  
  //print a new line space 
  System.out.println("\n"); 
  
  //for loop to print the sentences 
  for ( int i=0; i<=5; i++){
    randomInt = randomGenerator.nextInt(10) +1; 
    adjectiveGenerator(randomInt); //calls the adjective method 
    System.out.print ("The" + adjectiveGenerator(randomInt));
    
    randomInt = randomGenerator.nextInt(10)+ 1; 
    System.out.print (adjectiveGenerator(randomInt)); //prints the random adjective generated
    
    System.out.print(nounGenerator(randomInt)); //print the random noun generated 
    System.out.print (verbGenerator ( randomInt) +" the");
    randomInt = randomGenerator.nextInt(10) + 1;   
    System.out.print (adjectiveGenerator(randomInt));
    randomInt = randomGenerator.nextInt(10)+ 1;
    System.out.print(nounGenerator(randomInt));  
    System.out.println();
   }
  } //end of main method 
  
  //ADJECTIVES
  public static String adjectiveGenerator ( int randomInt ){
    Random randomGenerator = new Random () ;
    String adjective = "" ;
    //switch statement that chooses the adjective based on random number 
    switch (randomInt) {
      case 1:
        return "quick";
        break;
      case 2:
        return "slow";
        break;
      case 3:
        return "ugly";
        break;
      case 4:
        return "lazy";
        break;
      case 5:
        return "smelly";
        break;
      case 6: 
        return "funny";
        break;
      case 7:
        return "snarky";
        break;
      case 8:
        return "quirky";
        break;
      case 9:
        return "rude";
        break; 
      case 10:
        adjective = "kind";
        break; 
      default :
          return "Invalid pick";
    } //end of adj switch statement 
   return adjective;
  } // end of this method

  
  //NOUNS FOR SUBJECT
  public static String nounGenerator ( int randomInt ) {
    String adjective = "";
  switch(randomInt){ 
    case 1: 
      return "explanation"; 
    case 2: 
      return "security"; 
    case 3: 
       return "insect"; 
    case 4: 
       return "language";   
    case 5: 
       return "significance";       
    case 6: 
       return "category";   
    case 7: 
       return "tension";       
    case 8: 
       return "mood";     
    case 9: 
       return "society";   
    case 10: 
      return "feedback";  
    default: 
      return  "Invalid pick"; 
 
    }//end of switch statement 
  
  }//end of method

    
  //PAST TENSE VERBS
  public static String verbGenerator(int randomInt){
    String adjective = "";
    switch(randomInt){ 
       case 1: 
          return" greet"; 
       case 2: 
          return " catch"; 
       case 3: 
          return " behave"; 
       case 4: 
          return " vary"; 
       case 5: 
         return " emphasize"; 
       case 6: 
         return " allege";  
       case 7: 
         return " neglect";      
       case 8: 
         return " decrease";       
       case 9: 
         return " embody"; 
      case 10: 
         return " struggle";   
      default: 
         return  "Invalid pick"; 
     }//end of switch 
  }//end of method
    
  //NOUNS FOR OBJECT
  //public static {
    //Random randomGenerator = new Random () ;
    //int randomInt = randomGenerator.nextInt(10) ;
  //} //end of this method
    
  
  } //end of class
