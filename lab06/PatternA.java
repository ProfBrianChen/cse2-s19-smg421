//// Stella Garriga smg421
///// Lab 6 
///// 3/8/19

import java.util.Scanner; //imports scanner for use

public class PatternA { //class
  
  public static void main(String[] args){ //main method
    
    Scanner myScanner = new Scanner (System.in); //declaring the instance of the scanner and calling the instructor 
    
    System.out.println("Enter an integer between 1 and 10."); //prompts the user for an integer between 1 and 10 
    
    while (!myScanner.hasNextInt()){ //condition that indicates an error and constrution of first while loop
       System.out.println("Error: Enter an integer."); //indicates error to the user
       String junkWord = myScanner.next(); // flushes incorrect input from the scanner
    } //end of while loop for checking the input
    
    int i = myScanner.nextInt(); //declares the input from the user as the variable
    
    while (i<1 || i>10){ // checks if input is between 1 and 10
       System.out.println("Error: Enter an integer between 1 and 10.");
       i = myScanner.nextInt();
    }
      
    for (int j = 1; j <= i; j++){ //loop used for the pattern 
       for (int k = 1; k < j; k++){
          System.out.print(k + " ");
       } //end of inner loop
      System.out.println(j);
     } //end of outer loop
    
    
  } //end of method 
  
} // end of class 