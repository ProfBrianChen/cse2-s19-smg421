/////////////////////////////////
///// Stella Garriga smg421 /////
///////   CSE2 2/3/19    //////// 
/////////////////////////////////

public class Arithmetic{
  
  public static void main(String[] args) {
    // Prints arithmetic in the window 
    
    //Input Variables
    int numPants = 3; //Number of pairs of pants
    double pantsPrice = 34.98; //Cost per pair of pants 
    
    int numShirts = 2; //Number of shirts
    double shirtPrice = 24.99; //Price per shirt
    
    int numBelts = 1; //Number of belts
    double beltCost = 33.99; //Cost of each belt 
    
    double paSalesTax = 0.06; //the tax rate 
    
    //intermediate and output values 
    double TotalCostOfPants, TotalCostOfShirts, TotalCostOfBelts; //total costs of each article of clothing 
    double SalesTaxOfPants, SalesTaxOfShirts, SalesTaxOfBelts; //total amount of sales tax charged for each article of clothing
    double TotalCostOfPurchases; //total cost of all items of clothing 
    double TotalSalesTax; //total amount of sales tax charged on all the items
    double TotalPaidForTransaction; //total amount paid for the entire transaction, including sales tax
    
    //calculating and storing values 
    TotalCostOfPants=numPants*pantsPrice; //total cost of all pants 
    TotalCostOfShirts=numShirts*shirtPrice; //total cost of all shirts
    TotalCostOfBelts=numBelts*beltCost; //total cost of belt 
    
    SalesTaxOfPants=TotalCostOfPants*paSalesTax; //amount of sales tax charged for all pants 
    SalesTaxOfShirts=TotalCostOfShirts*paSalesTax; //amount of sales tax charged for all shirts
    SalesTaxOfBelts=TotalCostOfBelts*paSalesTax; //amount of sales tax charger for the belt 
    
    TotalCostOfPurchases=TotalCostOfPants+TotalCostOfShirts+TotalCostOfBelts; //amount spent on all of the clothing prior to adding in sales tax
    TotalSalesTax=SalesTaxOfPants+SalesTaxOfShirts+SalesTaxOfBelts; //amount of sales tax needed to be paid for all articles of clothing
    TotalPaidForTransaction=TotalCostOfPurchases+TotalSalesTax; //amount paid for transaction
    
    //eliminating all the extra decimal points to only keep 2 digits to the right of the decimal 
    int HundredSalesTaxOfPants= (int) (SalesTaxOfPants*100); //multiplying the sales tax charged on pants by 100 to move the decimal over two places and converting it into an integer
    double NicerSalesTaxOfPants=HundredSalesTaxOfPants/100.0; //converint the integer from the step above to a double so that the decimal only has 2 digits to the right
    
    int HundredSalesTaxOfShirts= (int) (SalesTaxOfShirts*100);
    double NicerSalesTaxOfShirts=HundredSalesTaxOfShirts/100.0;
    
    int HundredSalesTaxOfBelts= (int) (SalesTaxOfBelts*100);
    double NicerSalesTaxOfBelts=HundredSalesTaxOfBelts/100.0;
    
    int HundredTotalSalesTax= (int) (TotalSalesTax*100);
    double NicerTotalSalesTax=HundredTotalSalesTax/100.0;
    
    int HundredTotalPaidForTransaction= (int) (TotalPaidForTransaction*100);
    double NicerTotalPaidForTransaction=HundredTotalPaidForTransaction/100;
    
    //printing outputs
    System.out.println("The total cost of pants was "+TotalCostOfPants); // prints the total of the pants
    System.out.println("The sales tax charged on pants was "+NicerSalesTaxOfPants); //prints the nicer version of the total sales tax charged on pants
    System.out.println("The total cost of shirts was "+TotalCostOfShirts); //prints the total of the shirts
    System.out.println("The sales tax charged on shirts was "+NicerSalesTaxOfShirts); // prints the nicer total of the sales tax charged on shirts
    System.out.println("The total cost of belts was "+TotalCostOfBelts); // prints the total of the belts
    System.out.println("The sales tax charged on belts was "+NicerSalesTaxOfBelts); //prints the nicer total of the sales tax on the belt
    System.out.println("The total cost of the purchases before tax was "+TotalCostOfPurchases); // prints the total cost of the purchase prior to tax
    System.out.println("The total sales tax charged was "+NicerTotalSalesTax); // prints the nicer version of the total sales tax 
    System.out.println("The total paid for the transaction including tax was "+NicerTotalPaidForTransaction); // prints the nicer version of the total paid for the entire transaction
    
    
  }
  
}