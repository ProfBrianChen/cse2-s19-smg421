///////
//// CSE 02 Welcome Class
///
public class WelcomeClass{
  
  public static void main(String args[]){
    // Prints Welcome to the terminal window
    System.out.println("  -----------  ");
    System.out.println("  | Welcome |  ");
    System.out.println("  -----------  ");
    System.out.println("  ^  ^  ^  ^  ^  ^  ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-S--M--G--4--2--1->");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("  v  v  v  v  v  v  ");
    System.out.println("My name is Stella Garriga and I am a sophomore at Lehigh University majoring in Cognitive Science. I am Cuban and Dominican and live with my brither and mom in Perth Amboy, NJ.");
      
  }
  
}