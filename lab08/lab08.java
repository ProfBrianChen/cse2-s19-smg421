/// Stella Garriga smg421
/// Lab 08 due 3/5/18

import java.util.Arrays;
import java.util.Random; 

public class lab08 {
  
  //MAIN METHOD
  public static void main (String[] args) {
    
  //declare new random generator 
  Random randomGenerator = new Random(); 
  //initalize random generator 
  int randomSize = randomGenerator.nextInt(100); 
  while (randomSize<50) {
    randomSize = randomGenerator.nextInt(100);
  }
  int [] randomArray;
  randomArray = new int [randomSize]; 
  
  for (int i=0; i < randomSize; i++) {
    int randomInt= randomGenerator.nextInt(100);
    randomArray[i]= randomInt;
    System.out.print(randomArray[i] + "  ");
  } //end of for loop
    
  System.out.println();
  System.out.println();
  System.out.println("Range: " + getRange(randomArray));
  System.out.println("Mean: " + getMean(randomArray));
  System.out.println("Standard Deviation: " + getStDev(randomArray, getMean(randomArray)));
 
  } //end of main method 
  
  //RANGE METHOD 
  public static int getRange (int[] Ar) {
    
   Arrays.sort(Ar);
   System.out.println("The array sorted is: " );
    
   for(int i=0; i<Ar.length; i++){
      System.out.print(Ar[i]+"  ");
   }
    
   int range = Ar[Ar.length-1]-Ar[0];
   return range;
    
  } //end of range method 
  
  //MEAN METHOD
  public static double getMean (int[] Ar) {
   double sum=0.0;
   
   for (int i=0; i<Ar.length; i++){
     sum= sum+Ar[i];
   }
   
   double Avr=sum/(Ar.length-1);
   return Avr;
  
  } //end of mean method 
  
  //STANDARD DEVIATION METHOD 
  public static double getStDev (int[] Ar, double Mean) {
    double sum1 = 0.0;
    double [] num = new double [Ar.length];
    
    for(int i=0; i<Ar.length; i++){
      num[i]= (Ar[i]-Mean)*(Ar[i]-Mean);
      sum1 = sum1 + num[i];
    }
    
    double Std= Math.sqrt(sum1/Ar.length);
    return Std;
   
  } //end of standard deviation method 
  
} //end of class
