//// Stella Garriga smg421 
//// Homework #9 Due 4/16/19

import java.util.Scanner; //imports scanner 
import java.util.Arrays; //imports arrays
import java.util.Random; //imports random generator

public class ArrayGames { //class 
  
  //MAIN METHOD 
  public static void main (String[] args){
    
    Scanner myScanner = new Scanner (System.in); 
    System.out.println("Would you like to run insert or shorten?"); //prompts user for choice of shortening or inserting
    String userChoice = myScanner.nextLine(); //stores user input 
    
    String userInsert = "insert";
    String userShorten = "shorten"; 
   
    //INSERT
    if (userChoice.equals(userInsert)){
      
      //getting a random number between 10 and 20 to create the array
      Random randomGenerator = new Random(); //declare new random generator 
      int randomSize = randomGenerator.nextInt(20); //initalize random generator 
      while (randomSize<10) {
      randomSize = randomGenerator.nextInt(20);
      }
      int[] randomArray = generate(randomSize);
      print (randomArray) ; // calls on print method 
      
      //insertion part  
      System.out.println();
      System.out.println("This is the insert method!"); 
      //first input - creates a small array to be combined with the second small array
      int randomSize1 = randomGenerator.nextInt(10); //initalize random generator 
      while (randomSize1<5) {
      randomSize1 = randomGenerator.nextInt(10);
      }
      int[] randomArray1 = generate(randomSize1);
      System.out.println("Input 1 ");
      //second input - creates a small array to be combined with the first small array
      int randomSize2 = randomGenerator.nextInt(10); //initalize random generator 
      while (randomSize1<5) {
      randomSize1 = randomGenerator.nextInt(10);
      }
      int[] randomArray2 = generate(randomSize2);
      System.out.println("Input 2 ");
      //output - combines the two arrays by inserting one of the arrays into the spot of a member of the other 
      System.out.println("Output: ");     
      insert (randomArray1, randomArray2); 
        
        
    }//end of insert if statement 
      
    //SHORTEN
    if (userChoice.equals(userShorten)){
       
      //getting a random number between 10 and 20 to create the array
      Random randomGenerator = new Random(); //declare new random generator 
      int randomSize = randomGenerator.nextInt(20); //initalize random generator 
      while (randomSize<10) {
      randomSize = randomGenerator.nextInt(20);
      }
      int[] randomArray = generate(randomSize);
      print (randomArray) ; // calls on print method 
      
      //shorten part
      System.out.println();
      System.out.println("This is the shorten method!");
      //array for input 
      int randSize = randomGenerator.nextInt(20); //initalize random generator 
      while (randSize<10) {
      randSize = randomGenerator.nextInt(20);
      }
      int[] randomAr = generate(randSize);
      System.out.println("Input 1 ");
      //integer for input 
      System.out.println("Input an integer:"); //prompts user for choice of shortening or inserting
      int numChoice = myScanner.nextInt(); //stores user input 
      System.out.println("Input 2: " + numChoice);
      //output attained from the two inputs into method shorten
      System.out.println("Output: ");
      shorten (numChoice, randomAr);
    } //end of shorten if statement 
    
  } //end of main method 
  
  //GENERATE METHOD 
  public static int[] generate(int size){
    
    int [] randomVals = new int [size]; //initializing array
    
    Random randomGenerator = new Random(); //random generator is declared in this method 
    //for loop to create random numbers for the array 
    for (int i=0; i < size; i++) {
      int randomInt= randomGenerator.nextInt(size);
      randomVals[i]= randomInt;
      System.out.print(randomVals[i] + "  ");
    } //end of for loop
    
    return randomVals;
    
  }//end of generate method  
  
  //PRINT METHOD
  public static int[] print (int [] theArray) {
    return theArray;
   
  } //end of print method 
  
  //INSERT METHOD 
  public static int[] insert (int[] array1, int[] array2) {
    
    int fTerm = 0;
    int lTerm = array1.length - 1;
    int[] array3 = new int [array1.length + array2.length];
    
    //for loops to insert array 
    for (int i = 0; i<=array3.length; i++) { //tracks new array of combined length 
      int mTerm = (fTerm+lTerm)/2;
      //tracks first half of combined array
      if ( i<mTerm ){
        System.out.print(array1[i] + " ");
      }
      if ( i==mTerm ){
        System.out.print(Arrays.toString(array2) + "");
      }
      //tracks second half of the first arryay
      if ( i>mTerm) {
        System.out.print(array1[i-1] + " ");
      }
    }//end of for loop
   return array3; 
  }//end of insert method 
  
  //SHORTEN METHOD 
  public static int[] shorten (int num, int[] Ar) {
    
    int[] newAr = new int [Ar.length -1];
    for (int i=0; i < Ar.length; i++) {
      if (num == Ar[i]) {
   
        return newAr;
      }     
    } //end of for loop to check through array
    
   return Ar; 
    
  }// end of shorten method
  
} // end of class 