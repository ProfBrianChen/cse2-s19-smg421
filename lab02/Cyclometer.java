///////////////////
//// Stella Garriga smg421
/// CSE 2   Date: 2/1
/// This program should report the number of minutes and counts per trip in addition to the distance traveled in miles of each trip and the distance of the two trips combined. 

public class Cyclometer {
  // main method required for every Java program
  public static void main(String[] args) {
    
    // our input data
int secsTrip1=480; //Number of seconds that trip 1 took to later be converted into minutes 
int secsTrip2=3220; //Number of seconds that trip 2 took to later be converted to minutes 
int countsTrip1=1561; //Number of counts in trip 1
int countsTrip2=9037; //Number of counts in trip 2

//our intermediate variables and output data 
double wheelDiameter=27.0, //the diameter of the bicylce wheel that will allow you to calulcate the total distance later on
PI=3.14159, //the value of pi is defined here to be used in the calculations later on 
feetPerMile=5280, //defining the number of feet per mile for conversions 
inchesPerFoot=12, //defining the number of inches in each foot for conversions
secondsPerMinute=60; //dedfining the number of seconds per minute for later conversions
double distanceTrip1, distanceTrip2,totalDistance; //classifying the trips and total distance variables as doubles 
    
    //prints out the numbers that have been stored as variables and the seconds spent on each trip converted to minutes
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) +" minutes and had "+ countsTrip1+" counts."); //Prints how long trip 1 took in minutes and the number of counts for trip 1
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) +" minutes and had "+ countsTrip2+" counts."); //Prints how long trip 2 took in minutes and the number of counts for trip 2
    
    //run the calculation and store the values 
    // We are calculating
    distanceTrip1=countsTrip1*wheelDiameter*PI; // gives the distance traveled in inches
    distanceTrip1=inchesPerFoot*feetPerMile; //gives distance in miles 
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //takes the distance traveled in inches and converts it to the distance traveled in miles
    totalDistance=distanceTrip1+distanceTrip2;
    
    //print the output data 
    System.out.println("Trip 1 was "+distanceTrip1+" miles.");
    System.out.println("Trip 2 was "+distanceTrip2+" miles.");
    System.out.println("The total distance was "+totalDistance+" miles.");
    
  } //end of main method 
  
} //end of class