/////// Stella Garriga smg421
/////// hw 03  2/11/19
/// convert meters to inches 

import java.util.Scanner; //imports scanner

public class Convert{
  //main method
  public static void main(String[] args) {
    Scanner myScanner = new Scanner ( System.in ) ; //declares the instance of the class and constructs the instance
    
    System.out.print("Enter the distance in meters: "); //prompts the user to enter a distance in meters to be converted
    double meterDistance = myScanner.nextDouble () ; //accept the user input 
    
    double meterInches=meterDistance*39.3700787; //conversion of the meters to inches 
    
    
    System.out.println(meterDistance + " meters is " + meterInches + " inches."); //prints the conversion
    
  } //end of the main method
  
} //end of class