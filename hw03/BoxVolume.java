/////// Stella Garriga smg421
//////// hw03 2/12/19
//// prompts user for the dimension of the box, and the program then used those dimensions to calculate the volume 

import java.util.Scanner; //imports scanner

public class BoxVolume{
  //main method 
  public static void main(String[] args) {
    Scanner myScanner = new Scanner ( System.in ); //declares the instance of the class and constructs the instance 
    
    System.out.print("The width side of the box is: "); //prompts the user for the width of the box 
    double boxWidth = myScanner.nextDouble () ; //accepts the input from the scanner and stores it as the width
    
    System.out.print("The length of the box is: "); //prompts the user for the length of the box 
    double boxLength = myScanner.nextDouble () ; //accepts the input from the scanner and stores it as the length
    
    System.out.print("The height of the box is: "); //prompts the user for the height of the box
    double boxHeight = myScanner.nextDouble () ; //accepts the inout from the scanner and stores it as the height
    
    double boxVolume=boxWidth*boxLength*boxHeight; //calculates the volume of the box 
    System.out.println("The volume inside the box is " + boxVolume + " ."); //prints the volume of the box 
    
  } //end of main method 
 
} //end of class