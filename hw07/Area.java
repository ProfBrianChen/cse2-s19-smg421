/// Stella Garriga smg421@lehigh.edu
/// Due 3/26/19
/// Obj., program calculates the shape of 3 different shapes, enabling the user to choose which one they would like to have the program calc. 

import java.util.Scanner; //imports scanner for use 

public class Area { //beginning of class 
  
  //MAIN METHOD 
  public static void main (String []args) {
    
   Scanner myScanner = new Scanner ( System.in ) ; //declaring the instance of the scanner and calling the scanner instructor 
   
   System.out.println("Enter the name of a shape."); //prompts the user for a shape name 
   
   String shapeName = myScanner.nextLine();
   String triangle = "triangle";
   String rectangle = "rectangle";
   String circle = "circle";
    
   while (!shapeName.equals(rectangle) && !shapeName.equals(triangle) && !shapeName.equals(circle)){
        System.out.println("Error: That is not an acceptable shape. Enter either 'rectangle', 'triangle', or 'circle'.");
        shapeName = myScanner.nextLine();
   } //end of while loop
    
   if (shapeName.equals(rectangle)) { //if statement calls rectangle method
     double area = rectArea () ;
   }
   if (shapeName.equals(triangle)) { //if statement calls triangle method
     double area = triArea ();
   }
   if (shapeName.equals(circle)) { //if statement calls circle method
     double area = circArea ();
   }
    
    System.out.println(area);
    
  } //end of main method 
  
  
  //INPUT CHECK METHOD
  public static double inputCheck ( ) {
    
    Scanner myScanner = new Scanner ( System.in ) ; //declaring the instance of the scanner and calling the scanner instructor 
   
    while (!scan.hasNextDouble()){
     System.out.println("Error: Enter input in the form of a double.");
     String junkWord = myScanner.next(); 
    }
    
    double input = myScanner.nextDouble();
    
  } // end of input method

  
  //RECTANGLE METHOD
  public static double rectArea ( double length, double width ) {
     
    System.out.println("Enter the length of the rectangle"); //prompts the user for the length
    length = inputCheck ();
    
    System.out.println("Enter the width of the rectangle"); //prompts the user for the width
    width = inputCheck ();
    
    double rectArea = length * width ;
    
  } //end of rectangle method
  
  //TRIANGLE METHOD
  public static double triArea ( double base, double height ) {
    
    System.out.println("Enter the lenght of the base of the rectangle"); //prompts the user for the length of the base 
    base = inputCheck ();
    
    System.out.println("Enter the height of the rectangle"); //prompts the user for the height
    height = inputCheck () ;
    
    double triArea = (base*height)/2 ;
  } //end of triangle method 
  
  //CIRCLE METHOD 
  public static double circArea ( double radius ) {
    
    System.out.println("Enter the length of the radius"); //prompts the user for the length of the radius
    radius = inputCheck () ;
    
    double circArea = 3.14 * radius * radius ;
    
  }//end of circle method 
  
} //end of class 