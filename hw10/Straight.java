/////Stella Garriga smg421
//// Due 05/02/19

import java.util.Arrays; //imports arrays
import java.util.Random; //impot random number generator 
import java.util.Scanner; //imports scanner 


public class Straight {
  //MAIN METHOD
  public static void main (String[] args){ 
     
   //initializing the array of the deck of 52 cards 
   int[] deck = new int [53]; 
   //create the array of the deck of cards  
   System.out.println("Unshuffled: ");
   for (int i=0; i < deck.length; i++) {
     deck[i] = i;
     System.out.print(deck[i] + " "); 
   }
   //shows the shuffled deck of cards using the shuffle method 
   System.out.println();
   int[] sDeck = shuffle(deck);
   System.out.println("Shuffled: " + Arrays.toString(sDeck));
   //draws the first five cards of the shuffled deck 
   int [] fDeck = first5(sDeck);
   System.out.println("First five cards: " + Arrays.toString(fDeck));
   // k lowest number method 
   Scanner myScanner = new Scanner (System.in);
   System.out.println("Enter a k search term.");
   int lowTerm = myScanner.nextInt(); 
   int low = lowCard(lowTerm,fDeck);
   System.out.println("Lowest Card: " + low);
    
  } // end of main method 
    
  //SHUFFLE METHOD 
  public static int[] shuffle (int[] Ar) {
  
    for (int i=0; i<Ar.length; i++) {
      //pick a number in the array to switch with 
      int target = (int) (Math.random() * Ar.length);
      //swapping the values 
      int temp = Ar[target]; 
      Ar[target]=Ar[i];
      Ar[i]=temp;
    }
    return Ar;
  } //end of shuffle method
  
  //FIRST FIVE CARDS 
  public static int [] first5 (int[] Ar){
    //make a new array of length 5
    int [] result = new int [5];
    // for loop to select the numbers in the first five indeces of array
    int loc = 0;
    while (loc<5){
      result[loc]=Ar[loc];
      loc ++;
    }
    return result;
  } //end of first five method 
  
  //K METHOD
  public static int lowCard (int k, int[] Ar){
    //checks if input is between one and five 
    if (k<0 || k>5){
      System.out.println("Error"); 
    }
    //sorts the array in ascending order
    for (int i=0; i<Ar.length; i++){
      int j = i; 
      while (j>0 && Ar[j]<Ar[j-1]){
        int temp = Ar[j-1];
        Ar[j-1] = Ar[j];
        Ar[j] = temp;
      }
    }
    //checks for term equal to k 
    for (int i=0; i<Ar.length; i++){
      if (i-1 == k){
        return Ar[i-1];
      }
    }
    return -1;
  } //end of kth lowest number 
  
  //SEARCH METHOD 
  //public static straight (int[] Ar){
  
  //} //end of straight method 
  
  
  
} // end of class