//// Stella Garriga smg421
//// Due 05/02/2019

import java.util.Arrays; //imports arrays
import java.util.Random; //import random number generator 

public class RobotCity {
  
  //MAIN METHOD 
  public static void main (String[] args){
    //shows the east west + north south dimensions and city population
    System.out.println("East-West and North South Dimensions and Block Populations: " + buildCity());
    
  }// end of main method 
  
  //METHOD TO INITIALIZE CITY
  public static String buildCity () {
    //declare random generator 
    Random randomGenerator = new Random();
    //east west dimensions
    int ew = randomGenerator.nextInt(15);
    while (ew<10) {
    ew = randomGenerator.nextInt(15);
    }
    /*for (int i=0; i<ew; i++){ //assigns population to each block
      int pop = randomGenerator.nextInt(999);
      while (pop<100) {
        pop = randomGenerator.nextInt(999);
       }
      we[i]=pop;
    }*/
    //north south dimensions 
    int ns = randomGenerator.nextInt(15);
    while (ns<10) {
    ns = randomGenerator.nextInt(15);
    }
    //construct a multidimensional array with the dimensions
    int[][] cityInfo = new int [ew][ns];
    for (int i=0; i<ew; i++){
      for (int j=0; j<ns; j++){
        int pop = randomGenerator.nextInt(999);
        while (pop<100) {
          pop = randomGenerator.nextInt(999);
        }
        cityInfo[i][j] = pop;
      }
    }
    return Arrays.toString(cityInfo);
  } //end of buildcity method 
  
  //DISPLAY METHOD 
  //public static int[][] display (int[] Ar){
    
  //} //end of display method 
  
} //end of class 