/////// Stella Garriga smg421
////// HW 04 2/19/19
////// Obj: write a program that randomly draws five cards and detects whether the five cards contain a pair, 
/////two pair, or three of a kind - if none of these exist, the system will print out that the individual
////// has a high card hand.

import java.lang.*;
public class PokerHandCheck{
  
  public static void main(String[] args){
    //the main method 
    int cardNumber1 = (int)(Math.random()*52)+1; //produces a random integer for the first card
    int cardNumber2 = (int)(Math.random()*52)+1; //produces a random integer for the second card
    int cardNumber3 = (int)(Math.random()*52)+1; //produces a random integer for the third card
    int cardNumber4 = (int)(Math.random()*52)+1; //produces a random integer for the fourth card
    int cardNumber5 = (int)(Math.random()*52)+1; //produces a random integer for the fifth card
    
    String cardSuit1 = "";
      if (cardNumber1 >= 1 && cardNumber1 <= 13){ //provides the range in which the card would fall into the given suit for the first card
        cardSuit1 = "diamonds";
      }  //assigns the card suit diamonds
      else if (cardNumber1 >=14 && cardNumber1 <=26){
        cardSuit1 = "clubs";
      } //assigns the card suit clubs to that range of numbers 
      else if (cardNumber1 >=27 && cardNumber1 <=39){
        cardSuit1 = "hearts";
      }//assigns the card suit hearts 
      else if (cardNumber1 >=40 && cardNumber1 <=52){
        cardSuit1 = "spades";
      }//assigns the card suit spades 
      
    String cardIdentity1 = ""; //declares the cardIdentity as a string 
      switch (cardNumber1 % 13){ // swtich statement is used to compact the if statements that would be used to assign the identity, and a modulus is used to use the remainder to assign the identity to the first card
        case 1: 
          cardIdentity1 = "Ace";
          break;
        case 0: 
          cardIdentity1 = "King";
          break;
        case 11:
          cardIdentity1 = "Jack";
          break;
        case 12:
          cardIdentity1 = "Queen";
          break;
        default:
        cardIdentity1 = Integer.toString(cardNumber1 % 13);
      }//end of switch statement for the first card 
    
    
     String cardSuit2 = "";
      if (cardNumber2 >= 1 && cardNumber2 <= 13){ //provides the range in which the card would fall into the given suit for the second card
        cardSuit2 = "diamonds";
      }  //assigns the card suit diamonds
      else if (cardNumber2 >=14 && cardNumber2 <=26){
        cardSuit2 = "clubs";
      } //assigns the card suit clubs to that range of numbers 
      else if (cardNumber2 >=27 && cardNumber2 <=39){
        cardSuit2 = "hearts";
      }//assigns the card suit hearts 
      else if (cardNumber2 >=40 && cardNumber2 <=52){
        cardSuit2 = "spades";
      }//assigns the card suit spades 
      
    String cardIdentity2 = ""; //declares the cardIdentity as a string 
      switch (cardNumber2 % 13){ // swtich statement is used to compact the if statements that would be used to assign the identity, and a modulus is used to use the remainder to assign the identity to the second card
        case 1: 
          cardIdentity2 = "Ace";
          break;
        case 0: 
          cardIdentity2 = "King";
          break;
        case 11:
          cardIdentity2 = "Jack";
          break;
        case 12:
          cardIdentity2 = "Queen";
          break;
        default:
        cardIdentity2 = Integer.toString(cardNumber2 % 13);
      }//end of switch statement for the first card 
    
    
     String cardSuit3 = "";
      if (cardNumber3 >= 1 && cardNumber3 <= 13){ //provides the range in which the card would fall into the given suit for the third card
        cardSuit3 = "diamonds";
      }  //assigns the card suit diamonds
      else if (cardNumber3 >=14 && cardNumber3 <=26){
        cardSuit3 = "clubs";
      } //assigns the card suit clubs to that range of numbers 
      else if (cardNumber3 >=27 && cardNumber3 <=39){
        cardSuit3 = "hearts";
      }//assigns the card suit hearts 
      else if (cardNumber3 >=40 && cardNumber3 <=52){
        cardSuit3 = "spades";
      }//assigns the card suit spades 
      
    String cardIdentity3 = ""; //declares the cardIdentity as a string 
      switch (cardNumber3 % 13){ // swtich statement is used to compact the if statements that would be used to assign the identity, and a modulus is used to use the remainder to assign the identity to the third card
        case 1: 
          cardIdentity3 = "Ace";
          break;
        case 0: 
          cardIdentity3 = "King";
          break;
        case 11:
          cardIdentity3 = "Jack";
          break;
        case 12:
          cardIdentity3 = "Queen";
          break;
        default:
        cardIdentity3 = Integer.toString(cardNumber3 % 13);
      }//end of switch statement for the first card 
    
    
     String cardSuit4 = "";
      if (cardNumber4 >= 1 && cardNumber4 <= 13){ //provides the range in which the card would fall into the given suit for the fourth card
        cardSuit4 = "diamonds";
      }  //assigns the card suit diamonds
      else if (cardNumber4 >=14 && cardNumber4 <=26){
        cardSuit4 = "clubs";
      } //assigns the card suit clubs to that range of numbers 
      else if (cardNumber4 >=27 && cardNumber4 <=39){
        cardSuit4 = "hearts";
      }//assigns the card suit hearts 
      else if (cardNumber4 >=40 && cardNumber4 <=52){
        cardSuit4 = "spades";
      }//assigns the card suit spades 
      
    String cardIdentity4 = ""; //declares the cardIdentity as a string 
      switch (cardNumber4 % 13){ // swtich statement is used to compact the if statements that would be used to assign the identity, and a modulus is used to use the remainder to assign the identity to the fourth card
        case 1: 
          cardIdentity4 = "Ace";
          break;
        case 0: 
          cardIdentity4 = "King";
          break;
        case 11:
          cardIdentity4 = "Jack";
          break;
        case 12:
          cardIdentity4 = "Queen";
          break;
        default:
        cardIdentity4 = Integer.toString(cardNumber4 % 13);
      }//end of switch statement for the first card 
    
    
     String cardSuit5 = "";
      if (cardNumber5 >= 1 && cardNumber5 <= 13){ //provides the range in which the card would fall into the given suit for the fifth card
        cardSuit5 = "diamonds";
      }  //assigns the card suit diamonds
      else if (cardNumber5 >=14 && cardNumber5 <=26){
        cardSuit5 = "clubs";
      } //assigns the card suit clubs to that range of numbers 
      else if (cardNumber5 >=27 && cardNumber5 <=39){
        cardSuit5 = "hearts";
      }//assigns the card suit hearts 
      else if (cardNumber5 >=40 && cardNumber5 <=52){
        cardSuit5 = "spades";
      }//assigns the card suit spades 
      
    String cardIdentity5 = ""; //declares the cardIdentity as a string 
      switch (cardNumber5 % 13){ // swtich statement is used to compact the if statements that would be used to assign the identity, and a modulus is used to use the remainder to assign the identity to the first card
        case 1: 
          cardIdentity5 = "Ace";
          break;
        case 0: 
          cardIdentity5 = "King";
          break;
        case 11:
          cardIdentity5 = "Jack";
          break;
        case 12:
          cardIdentity5 = "Queen";
          break;
        default:
        cardIdentity5 = Integer.toString(cardNumber5 % 13);
      }//end of switch statement for the first card 
    
  
    String cardCombo = ""; //declares the specific possible combinations as a string 
      if (cardNumber1%13==cardNumber2%13){//sets the condition in which the hand contains a pair
        cardCombo = "a pair";
      }
      else if (cardNumber1%13==cardNumber3%13){
        cardCombo = "a pair";
      }
      else if (cardNumber1%13==cardNumber4%13){
        cardCombo = "a pair";
      }
      else if (cardNumber1%13==cardNumber5%13){
        cardCombo = "a pair";
      }
      else if (cardNumber2%13==cardNumber1%13){
        cardCombo = "a pair";
      }
      else if (cardNumber2%13==cardNumber3%13){
        cardCombo = "a pair";
      }
      else if (cardNumber2%13==cardNumber4%13){
        cardCombo = "a pair";
      }
      else if (cardNumber2%13==cardNumber5%13){
        cardCombo = "a pair";
      }
      else if (cardNumber3%13==cardNumber1%13){
        cardCombo = "a pair";
      }
      else if (cardNumber3%13==cardNumber2%13){
        cardCombo = "a pair";
      }
      else if (cardNumber3%13==cardNumber4%13){
        cardCombo = "a pair";
      }
      else if (cardNumber3%13==cardNumber5%13){
        cardCombo = "a pair";
      }
      else if (cardNumber4%13==cardNumber1%13){
        cardCombo = "a pair";
      }
      else if (cardNumber4%13==cardNumber2%13){
        cardCombo = "a pair";
      }
      else if (cardNumber4%13==cardNumber3%13){
        cardCombo = "a pair";
      }
      else if (cardNumber4%13==cardNumber5%13){
        cardCombo = "a pair";
      }
      else if (cardNumber5%13==cardNumber1%13){
        cardCombo = "a pair";
      }
      else if (cardNumber5%13==cardNumber2%13){
        cardCombo = "a pair";
      }
      else if (cardNumber5%13==cardNumber3%13){
        cardCombo = "a pair";
      }
      else if (cardNumber5%13==cardNumber4%13){
        cardCombo = "a pair";
      }
      else if (cardNumber1%13==cardNumber2%13 && cardNumber3%13==cardNumber4%13){ //sets the condition for what is to be considered a two pair
        cardCombo = "a two pair";
      }
      else if (cardNumber1%13==cardNumber2%13 && cardNumber3%13==cardNumber5%13){
        cardCombo = "a two pair";
      }
      else if (cardNumber1%13==cardNumber2%13 && cardNumber4%13==cardNumber5%13){
        cardCombo = "a two pair";
      }
      else if (cardNumber2%13==cardNumber3%13 && cardNumber1%13==cardNumber4%13){
        cardCombo = "a two pair";
      }
      else if (cardNumber2%13==cardNumber3%13 && cardNumber1%13==cardNumber5%13){
        cardCombo = "a two pair";
      }
      else if (cardNumber2%13==cardNumber3%13 && cardNumber4%13==cardNumber5%13){
        cardCombo = "a two pair";
      }
      else if (cardNumber3%13==cardNumber4%13 && cardNumber1%13==cardNumber5%13){
        cardCombo = "a two pair";
      }
      else if (cardNumber3%13==cardNumber4%13 && cardNumber2%13==cardNumber5%13){
        cardCombo = "a two pair";
      }
      else if (cardNumber4%13==cardNumber5%13 && cardNumber1%13==cardNumber3%13){
        cardCombo = "a two pair";
      }
      else if (cardNumber1%13==cardNumber2%13 && cardNumber2%13==cardNumber3%13){
        cardCombo = "three of a kind";
      }
      else if (cardNumber1%13==cardNumber2%13 && cardNumber2%13==cardNumber4%13){
        cardCombo = "three of a kind";
      }
      else if (cardNumber1%13==cardNumber2%13 && cardNumber2%13==cardNumber5%13){
        cardCombo = "three of a kind";
      }
      else if (cardNumber2%13==cardNumber3%13 && cardNumber3%13==cardNumber4%13){
        cardCombo = "three of a kind";
      }
      else if (cardNumber2%13==cardNumber3%13 && cardNumber3%13==cardNumber5%13){
        cardCombo = "three of a kind";
      }
      else if (cardNumber3%13==cardNumber4%13 && cardNumber4%13==cardNumber5%13){
        cardCombo = "three of a kind";
      }
      else if (cardNumber1%13==cardNumber3%13 && cardNumber3%13==cardNumber5%13){
        cardCombo = "three of a kind";
      }
      else if (cardNumber1%13==cardNumber3%13 && cardNumber3%13==cardNumber5%13){
        cardCombo = "three of a kind";
      }
      else if (cardNumber1%13==cardNumber3%13 && cardNumber3%13==cardNumber4%13){
        cardCombo = "three of a kind";
      }
      else if (cardNumber1%13==cardNumber4%13 && cardNumber4%13==cardNumber5%13){
        cardCombo = "three of a kind";
      }
     else {
       cardCombo = "a high card hand";
     }
    
    
    System.out.println("Your random cards were: ");
    System.out.println("   the "+ cardIdentity1 + " of " + cardSuit1 );
    System.out.println("   the "+ cardIdentity2 + " of " + cardSuit2 );
    System.out.println("   the "+ cardIdentity3 + " of " + cardSuit3 );
    System.out.println("   the "+ cardIdentity4 + " of " + cardSuit4 );
    System.out.println("   the "+ cardIdentity5 + " of " + cardSuit5 );
    System.out.println("You have " + cardCombo + "!");
    
  }// end of main method 
} //end of class