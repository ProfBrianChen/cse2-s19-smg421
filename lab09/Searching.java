//// Stella Garriga smg421
//// lab 09 4/12/19

import java.util.Scanner;
import java.util.Arrays;  
import java.util.Random;

public class Searching {
  
  //MAIN METHOD 
  public static void main (String[] args) {
    
    Scanner myScanner = new Scanner (System.in);
    System.out.println("Would you like to perform a linear search or a binary search?");
    String searchType = myScanner.nextLine(); 
    
    String binaryEnt = "binary";
    String linearEnt = "linear";
    
    if (searchType.equals(binaryEnt)){
        System.out.println("Enter the size of the array.");
        int arraySizeb = myScanner.nextInt(); 
        int [] myArrayb = binArray(arraySizeb);
        Arrays.sort(myArrayb);
        System.out.println(Arrays.toString(myArrayb));
      
        System.out.println("Enter the integer search term.");
        int searchTermb = myScanner.nextInt();
        System.out.println("Index where the search term is found: " + binSearch(searchTermb, myArrayb));
        
    }
    
    if (searchType.equals(linearEnt)){
      
        System.out.println("Enter the size of the array.");
        int arraySizel = myScanner.nextInt(); 
        int [] myArray = linArray(arraySizel);
        System.out.println(myArray);
      
        System.out.println("Enter the integer search term.");
        int searchTerml = myScanner.nextInt();
        System.out.println("Index where the search term is found: " + linSearch(searchTerml, myArray));
    }
    
  } //end of main method 
  
  //ARRAY 1 METHOD
  public static int[] linArray (int size) {
    int [] randomVals;
    randomVals = new int [size]; 
    
    Random randomGenerator = new Random();
    for (int i=0; i < size; i++) {
      int randomInt= randomGenerator.nextInt(size);
      randomVals[i]= randomInt;
      System.out.print(randomVals[i] + "  ");
    } //end of for loop
    
    return randomVals;
 
  } //end of linear search array generation

  //ASCENDING ARRAY GENERATION METHOD 
  public static int[] binArray (int size) {
    int [] randomVals;
    randomVals = new int [size]; 
    
    Random randomGenerator = new Random();
    for (int i=0; i < size; i++) {
      int randomInt= randomGenerator.nextInt(size);
      randomVals[i]= randomInt;
      System.out.print(randomVals[i] + "  ");
    } //end of for loop
    
    return randomVals;
    
  } //end of method to create an ascending array for binary search
  
  //METHOD TO LINEAR SEARCH FOR SINGLE INTEGER 
  public static int linSearch (int sTerm, int[] anyArray) {
    
    for (int i=0; i < anyArray.length; i++) {
      if (sTerm == anyArray[i]) {
        return i ;
      }// end of if statement       
    } //end of for loop to check through array 
    
    return -1; 
    
  } //end of linear search method 
  
  //METHOD TO BINARY SEARCH FOR SINGLE INTEGER 
  public static int binSearch (int sTerm, int[] anyArray){
    int fTerm = 0;
    int lTerm = anyArray.length - 1; 
    
    while (fTerm <= lTerm){
      int mTerm = (fTerm+lTerm)/2 ;
      if (anyArray[mTerm] < sTerm) {
        fTerm = mTerm + 1;
      } //end of if statement 
      else if (anyArray[mTerm] > sTerm){
        lTerm = mTerm - 1;
      }//end of else if statement 
      else {
        return mTerm; 
      } //end of else 
    } //end of while loop
    
    return -(fTerm + 1);
    
  } //end of binary search method
  
  
} // end of class 