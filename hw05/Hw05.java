///// Stella Garriga smg421
///// Hw 05 Due 3/6/19
///// objective: ask the user to enter information relating to a course they are currently taking 
////and check to make sure that what the user enters is actually of the correct type, and if not, use an infinite loop to ask again

import java.util.Scanner; //imports scanner

public class Hw05{ //beginning class 
  
  public static void main(String[] args){ //beginning main method 
    
    Scanner scan = new Scanner (System.in); //declaring the instance of the scanner and calling the instructor 
    
    // Course number
    System.out.println("Enter the course number of a course you are taking."); //prompts the user for the course number 
    
    while (!scan.hasNextInt()){ //begins the while loop, while setting the boolean condition to check for an integer
      System.out.println("Enter the course number in the form of an integer."); //asks the user for an integer again because it was not before
      String junkWord = scan.next(); //flushes the incorrect input from the scanner
    } //end of while loop
    
    int courseNum = scan.nextInt(); //stores the int for the course number 
    
    //Department name
    System.out.println("Enter the department name of the course."); //prompts the user for the department name for the course 
    
    while (scan.hasNextInt() || scan.hasNextDouble()){
      System.out.println("Enter the department name in the form of a string.");
      String junkWord = scan.next(); 
    }
    
    String depName = scan.next();
    
    //Number of times a week the class meets 
    System.out.println("Enter the number of times per week the class meets."); // prompts the user for the number of times the class meets each week
    
    while (!scan.hasNextInt()){
      System.out.println("Enter the number of times in the form of an integer.");
      String junkWord = scan.next(); 
    }
    
    int courseFreq = scan.nextInt();
    
    //Time the class starts
    System.out.println("Enter the time the class starts."); //prompts the user for time at which the class begins
    
    while (!scan.hasNextInt()){
      System.out.println("Enter the time in the form of an integer.");
      String junkWord = scan.next(); 
    }
    
    int courseTime = scan.nextInt();
    
    //Instructor name
    System.out.println("Enter the last name of the instructor of the course."); //prompts the user for just the last name of the instructor of the course
    
    while (scan.hasNextInt() || scan.hasNextDouble()){
      System.out.println("Enter the instructor name in the form of a string.");
      String junkWord = scan.next(); 
    }
    
    String profName = scan.next();
    
    //Number of students in the course
    System.out.println("Enter the number of students in the course."); //prompts the user for the number of students in the class
    
    while (!scan.hasNextInt()){
      System.out.println("Enter the number of students in the form of an integer.");
      String junkWord = scan.next(); 
    }
    
    int classSize = scan.nextInt();
    
    //System.out.println("department name: " + depName);
    
  } //end of main method 
  
} // end of class